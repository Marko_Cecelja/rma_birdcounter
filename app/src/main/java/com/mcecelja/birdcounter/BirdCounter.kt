package com.mcecelja.birdcounter

import android.app.Application
import android.content.Context

class BirdCounter : Application() {

    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}