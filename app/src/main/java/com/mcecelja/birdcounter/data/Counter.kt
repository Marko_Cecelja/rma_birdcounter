package com.mcecelja.birdcounter.data

class Counter(private var birdCount: Int, private var color: Int) {

    fun getBirdCount() = birdCount
    fun getBirdColor() = color

    fun countBird(colorCode: Int) {
        this.birdCount++
        this.color = colorCode
    }
}