package com.mcecelja.birdcounter.data

import android.content.Context
import com.mcecelja.birdcounter.BirdCounter
import com.mcecelja.birdcounter.enums.ColorEnum

class PreferenceManager {

    companion object {
        private val PREFS_FILE = "preferences"

        private val KEY_BIRD_COUNT = "birdCount"

        private val KEY_BIRD_COLOR = "birdColor"

        fun getBirdCounter(): Counter {

            val sharedPreferences = BirdCounter.context.getSharedPreferences(
                PREFS_FILE, Context.MODE_PRIVATE
            )

            return Counter(
                sharedPreferences.getInt(KEY_BIRD_COUNT, 0),
                sharedPreferences.getInt(KEY_BIRD_COLOR, ColorEnum.BLACK.code)
            )
        }

        fun saveBirdCounter(counter: Counter) {
            BirdCounter.context.getSharedPreferences(
                PREFS_FILE, Context.MODE_PRIVATE
            )
                .edit()
                .putInt(KEY_BIRD_COUNT, counter.getBirdCount())
                .putInt(KEY_BIRD_COLOR, counter.getBirdColor())
                .apply()
        }
    }
}