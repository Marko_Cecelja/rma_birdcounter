package com.mcecelja.birdcounter.enums

import com.mcecelja.birdcounter.R

enum class ColorEnum(val code: Int) {

    RED(R.color.bird_red),
    BROWN(R.color.bird_brown),
    ORANGE(R.color.bird_orange),
    CYAN(R.color.bird_cyan),
    BLACK(R.color.black);
}