package com.mcecelja.birdcounter.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.mcecelja.birdcounter.BirdCounter
import com.mcecelja.birdcounter.data.Counter
import com.mcecelja.birdcounter.data.PreferenceManager
import com.mcecelja.birdcounter.databinding.ActivityMainBinding
import com.mcecelja.birdcounter.enums.ColorEnum
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var counter = PreferenceManager.getBirdCounter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUI()

        binding.bOrange.setOnClickListener { countBird(ColorEnum.ORANGE.code) }
        binding.bRed.setOnClickListener { countBird(ColorEnum.RED.code) }
        binding.bCyan.setOnClickListener { countBird(ColorEnum.CYAN.code) }
        binding.bBrown.setOnClickListener { countBird(ColorEnum.BROWN.code) }

        binding.bReset.setOnClickListener { reset() }
    }

    private fun countBird(colorCode: Int) {
        counter.countBird(colorCode)
        PreferenceManager.saveBirdCounter(counter)
        setUI()
    }

    private fun reset() {
        counter = Counter(0, ColorEnum.BLACK.code)
        setUI()
        PreferenceManager.saveBirdCounter(counter)
    }

    private fun setUI() {
        binding.tvBirdCount.text = counter.getBirdCount().toString()
        binding.tvBirdCount.setTextColor(ContextCompat.getColor(BirdCounter.context, counter.getBirdColor()))
    }
}